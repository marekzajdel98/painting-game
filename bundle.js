(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
const colors = ["#fafafa", "#c8c8c8", "#969696", "#646464", "#000000"];

////////////////Rectangle class
class Rectangle {
  constructor(X, Y, color) {
    this.X = X;
    this.Y = Y;
    this.color = color;
  }
}

////////////////Functions
function colorParser(colorID) {
  return colors[colorID];
}

function downloadJSONFile(content, fileName, contentType) {
  let a = document.createElement("a");
  let file = new Blob([content], { type: contentType });
  a.href = URL.createObjectURL(file);
  a.download = fileName;
  a.click();
}

function readSingleFile(e) {
  const file = e.target.files[0];
  if (!file) {
    return;
  }
  let reader = new FileReader();
  reader.onload = function(e) {
    let contents = e.target.result;
    vue.fileContent = contents;
    vue.fileIsChosen = true;
  };
  reader.readAsText(file);
}

/////////////////Vue Instance
const vue = new Vue({
  el: "#app",
  data: {
    rectangles: [],
    fileIsChosen: false,
    error: ""
  },
  created: function() {
    //Initialize the matrix
    for (let i = 0; i < 10; i++) {
      for (let j = 0; j < 10; j++) {
        this.rectangles.push(new Rectangle(i, j, 0));
      }
    }
    //Add listener for the file input
    document
      .getElementById("file-input")
      .addEventListener("change", readSingleFile, false);
  },
  methods: {
    changeRectangleColor(index) {
      let rectangle = this.rectangles[index];
      rectangle.color++;
      if (rectangle.color > 4) rectangle.color = 0;

      //Select element by its ID set by Vue binding and change it's color
      document.querySelector(
        `#rectangle_${index}`
      ).style.backgroundColor = colorParser(rectangle.color);
    },
    saveJSON() {
      let parsedRectangles = JSON.stringify(this.rectangles);
      downloadJSONFile(parsedRectangles, "json.txt", "text/plain");
    },
    restartGame() {
      for (let index = 0; index < 100; index++) {
        this.rectangles[index].color = 0;
        document.querySelector(
          `#rectangle_${index}`
        ).style.backgroundColor = colorParser(0);
      }
      this.error = "";
    },
    loadJSON() {
      this.error = "";
      try {
        this.fileContent = JSON.parse(this.fileContent);
        for (let index = 0; index < 100; index++) {
          this.rectangles[index].color = this.fileContent[index].color;
          document.querySelector(
            `#rectangle_${index}`
          ).style.backgroundColor = colorParser(this.fileContent[index].color);
        }
      } catch {
        this.error = "Invalid input file. Is it in a JSON format?";
      }
    }
  }
});

module.exports = {
  colorParser
};

},{}]},{},[1]);
