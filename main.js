//Function to read file and set vue variables
function readSingleFile(e) {
  const file = e.target.files[0];
  if (!file) {
    return;
  }
  let reader = new FileReader();
  reader.onload = function(e) {
    let contents = e.target.result;
    vue.fileContent = contents;
    vue.fileIsChosen = true;
  };
  reader.readAsText(file);
}

/////////////////Vue Instance
const utils = require("./utils");
const vue = new Vue({
  el: "#app",
  data: {
    rectangles: [],
    fileIsChosen: false,
    error: ""
  },
  created: function() {
    //Initialize the matrix
    for (let i = 0; i < 10; i++) {
      for (let j = 0; j < 10; j++) {
        this.rectangles.push(new Rectangle(i, j, 0));
      }
    }
    //Add listener for the file input
    document
      .getElementById("file-input")
      .addEventListener("change", readSingleFile, false);
  },
  methods: {
    changeRectangleColor(index) {
      let rectangle = this.rectangles[index];
      rectangle.color++;
      if (rectangle.color > 4) rectangle.color = 0;

      //Select element by its ID set by Vue binding and change it's color
      document.querySelector(
        `#rectangle_${index}`
      ).style.backgroundColor = colorParser(rectangle.color);
    },
    saveJSON() {
      let parsedRectangles = JSON.stringify(this.rectangles);
      downloadJSONFile(parsedRectangles, "json.txt", "text/plain");
    },
    restartGame() {
      for (let index = 0; index < 100; index++) {
        this.rectangles[index].color = 0;
        document.querySelector(
          `#rectangle_${index}`
        ).style.backgroundColor = colorParser(0);
      }
      this.error = "";
    },
    loadJSON() {
      this.error = "";
      try {
        this.fileContent = JSON.parse(this.fileContent);
        for (let index = 0; index < 100; index++) {
          this.rectangles[index].color = this.fileContent[index].color;
          document.querySelector(
            `#rectangle_${index}`
          ).style.backgroundColor = colorParser(this.fileContent[index].color);
        }
      } catch {
        this.error = "Invalid input file. Is it in a JSON format?";
      }
    }
  }
});
