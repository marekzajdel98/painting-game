const colors = ["#fafafa", "#c8c8c8", "#969696", "#646464", "#000000"];

////////////////Rectangle class
class Rectangle {
  constructor(X, Y, color) {
    this.X = X;
    this.Y = Y;
    this.color = color;
  }
}

////////////////Functions
function colorParser(colorID) {
  return colors[colorID];
}

function downloadJSONFile(content, fileName, contentType) {
  let a = document.createElement("a");
  let file = new Blob([content], { type: contentType });
  a.href = URL.createObjectURL(file);
  a.download = fileName;
  a.click();
}

module.exports = {
  colorParser
};
