const assert = require("chai").assert;
const colorParser = require("../utils").colorParser;

describe("Utils", function() {
  it("Color with ID '0' should be parsed to '#fafafa'", function() {
    assert.equal(colorParser(0), "#fafafa");
  });
});
